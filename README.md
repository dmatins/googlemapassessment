# README

Google Maps Assessment for Iterative Computing
==============

An interactive tool that calculates and displays the distance and travel time between two locations that the user inputs using the Google Maps API.

Created using the Ruby on Rails Starter App: https://github.com/RailsApps/rails-composer
The key used for the Google Maps api only works with http://localhost:3000

* Ruby version 2.3.0
* Rails version 5.1.0


To Run
--------------

- bundle install
- rails server
- visit http://localhost:3000/ to view and interact

